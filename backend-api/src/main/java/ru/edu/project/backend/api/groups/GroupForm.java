package ru.edu.project.backend.api.groups;

import lombok.Builder;
import lombok.Getter;
import lombok.extern.jackson.Jacksonized;

@Getter
@Builder
@Jacksonized
public class GroupForm {

    /**
     * id группы.
     */
    private Long groupId;

    /**
     * Название группы.
     */
    private String name;

    /**
     * Описание.
     */
    private String description;
}
