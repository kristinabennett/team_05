package ru.edu.project.backend.model;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.jackson.Jacksonized;

import java.sql.Timestamp;
import java.util.List;

@Getter
@Setter
@Jacksonized
@Builder
public class Task {

    /**
     * id задания.
     */
    private Long id;

    /**
     * id студента.
     */
    private Long studentId;

    /**
     * Статус.
     */
    private String status;

    /**
     * Время создания задания.
     */
    private Timestamp createdAt;

    /**
     * Дата сдачи контрольных работ.
     */
    private Timestamp deadLine;

    /**
     * Комментарий преподавателя.
     */
    private String comment;

    /**
     * parent id.
     *
     * null - это основное задание,
     * not null - это предмет
     */
    private Long parentId;

    /**
     * Наименование основного задания, либо предмета.
     */
    private String taskSubtask;

    /**
     * Выставленная оценка.
     *
     * null для основного задания,
     * not null для предмета
     */
    private Long assessment;

    /**
     * Список заданий.
     */
    private List<Task> subTasks;


}
