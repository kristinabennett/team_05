package ru.edu.project.backend.da.jpa.entity;

import lombok.Getter;
import lombok.Setter;
import ru.edu.project.backend.api.students.StudentAbstract;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Getter
@Setter
@Table(name = "student")
public class StudentEntity implements StudentAbstract {

    /**
     * id.
     */
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE,
            generator = "student_seq")
    @SequenceGenerator(name = "student_seq", sequenceName = "student_id_sequence", allocationSize = 1)
    private Long id;

    /**
     * group_id.
     */
    @Column (name = "group_id")
    private Long groupId;

    /**
     * record_id.
     */
    @Column (name = "record_id")
    private Long recordId;

    /**
     * name.
     */
    private String name;

    /**
     * second_name.
     */
    @Column (name = "second_name")
    private String secondName;

    /**
     * last_name.
     */
    @Column (name = "last_name")
    private String lastName;

    /**
     * birthday_date.
     */
    @Column (name = "birthday_date")
    private String birthdayDate;

    /**
     * phone.
     */
    private String phone;

    /**
     * entry_date.
     */
    @Column (name = "entry_date")
    private String entryDate;

}
