package ru.edu.project.backend.da.jdbctemplate;

import lombok.SneakyThrows;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.simple.SimpleJdbcInsert;
import org.springframework.stereotype.Service;
import ru.edu.project.backend.api.groups.GroupInfo;
import ru.edu.project.backend.da.GroupDALayer;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
@Profile("JDBC_TEMPLATE")
public class GroupDA implements GroupDALayer {

    /**
     * Запрос для вывода всех групп.
     */
    public static final String QUERY_ALL_GROUPS = "SELECT * FROM students_group";

    /**
     * Запрос для поиска группы по номеру.
     */
    public static final String QUERY_FOR_GROUP_ID = "SELECT * FROM students_group WHERE group_id = ?";

    /**
     * Обновление данных студента.
     * Обновляем только изменяемые поля.
     */
    public static final String QUERY_FOR_UPDATE = "UPDATE students_group SET name = :name, description = :description WHERE group_id = :group_id";

    /**
     * Зависимость на шаблон jdbc.
     */
    @Autowired
    private JdbcTemplate jdbcTemplate;

    /**
     * Зависимость на шаблон jdbc insert.
     */
    private SimpleJdbcInsert jdbcInsert;

    /**
     * Зависимость на шаблон jdbc named.
     */
    @Autowired
    private NamedParameterJdbcTemplate jdbcNamed;

    /**
     * Внедрение зависимости jdbc insert с настройкой для таблицы.
     *
     * @param bean
     */
    @Autowired
    public void setJdbcInsert(final SimpleJdbcInsert bean) {
        jdbcInsert = bean
                .withTableName("students_group")
                .usingGeneratedKeyColumns("group_id");

    }

    /**
     * Получение данных группы по номеру.
     *
     * @param groupId
     * @return student
     */
    @Override
    public GroupInfo getById(final Long groupId) {
        return jdbcTemplate.query(QUERY_FOR_GROUP_ID, this::singleRowMapper, groupId);
    }

    /**
     * Сохранение (создание/обновление) группы.
     *
     * @param draft
     * @return group
     */
    @Override
    public GroupInfo save(final GroupInfo draft) {
        if (draft.getGroupId() != null) {
            return update(draft);
        } else {
            return insert(draft);
        }
    }

    /**
     * Получение списка групп.
     *
     * @return list groups
     */
    @Override
    public List<GroupInfo> getAll() {
        return jdbcTemplate.query(QUERY_ALL_GROUPS, this::rowMapper);
    }

    private GroupInfo update(final GroupInfo draft) {
        jdbcNamed.update(QUERY_FOR_UPDATE, toMap(draft));
        return draft;
    }


    private GroupInfo insert(final GroupInfo draft) {
        long id = jdbcInsert.executeAndReturnKey(toMap(draft)).longValue();
        draft.setGroupId(id);
        return draft;
    }

    private Map<String, Object> toMap(final GroupInfo draft) {
        HashMap<String, Object> map = new HashMap<>();
        if (draft.getGroupId() != null) {
            map.put("group_id", draft.getGroupId());
        }

        map.put("name", draft.getName());
        map.put("description", draft.getDescription());

        return map;
    }


    @SneakyThrows
    private GroupInfo rowMapper(final ResultSet rs, final int pos) {
        return mapRow(rs);
    }

    @SneakyThrows
    private GroupInfo singleRowMapper(final ResultSet rs) {
        rs.next();
        return mapRow(rs);
    }

    private GroupInfo mapRow(final ResultSet rs) throws SQLException {
        return GroupInfo.builder()
                .groupId(rs.getLong("group_id"))
                .name(rs.getString("name"))
                .description(rs.getString("description"))
                .build();
    }
}
