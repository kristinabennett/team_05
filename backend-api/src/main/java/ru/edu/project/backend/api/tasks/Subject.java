package ru.edu.project.backend.api.tasks;

import lombok.Getter;
import lombok.Setter;
import lombok.extern.jackson.Jacksonized;

@Getter
@Setter
@Jacksonized
public class Subject {

    /**
     * Конструктор.
     */
    public Subject() {
    }

    /**
     * Код предмета.
     */
    private Long id;

    /**
     * Название предмета.
     */
    private String title;

    /**
     * Описание предмета.
     */
    private String desc;

    /**
     * Оценка за предмет.
     */
    private Long assessment;

    /**
     * Копирование объектов.
     * @param another - Subject, из которого копируем
     */
    public void copy(final Subject another) {
        this.id = another.getId();
        this.title = another.getTitle();
        this.desc = another.getDesc();
        this.assessment = another.getAssessment();
    }

}
