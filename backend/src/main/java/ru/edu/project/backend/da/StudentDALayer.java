package ru.edu.project.backend.da;

import ru.edu.project.backend.api.students.StudentInfo;

import java.util.List;

public interface StudentDALayer {
    /**
     * Получение данных студента по номеру зачетки.
     *
     * @param recordId
     * @return student
     */
    StudentInfo getById(Long recordId);

    /**
     * Сохранение (создание/обновление) данных студента.
     *
     * @param draft
     * @return student
     */
    StudentInfo save(StudentInfo draft);

    /**
     * Получение списка студентов.
     *
     * @return list students
     */
    List<StudentInfo> getAll();

    /**
     * Получение списка студентов по номеру группы.
     * @param groupId
     * @return list students
     */
    List<StudentInfo> getStudentsByGroup(Long groupId);
}
