package ru.edu.project.frontend.controller;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;
import ru.edu.project.backend.api.groups.GroupService;
import ru.edu.project.backend.api.students.StudentInfo;
import ru.edu.project.backend.api.students.StudentService;
import ru.edu.project.backend.api.tasks.TaskInfo;
import ru.edu.project.backend.api.tasks.TaskService;

@RequestMapping("/report")
@Controller
public class ReportController {

    /**
     * Ссылка на сервис студентов.
     */
    @Autowired
    private StudentService studentService;

    /**
     * Ссылка на сервис групп.
     */
    @Autowired
    private GroupService groupService;

    /**
     * Ссылка на сервис заданий.
     */
    @Autowired
    private TaskService taskService;

    /**
     * Отображение списка студентов.
     *
     * @param model
     * @return path
     */
    @GetMapping("/")
    public String index(final Model model) {

        model.addAttribute("students", studentService.getAllStudents());


        return "report/index";
    }

    /**
     * Отображение информации по студенту по номеру зачетки.
     * @param recordId
     * @return ModelAndView
     */
    @GetMapping("/view/{id}")
    public ModelAndView view(final @PathVariable("id") Long recordId) {
        ModelAndView model = new ModelAndView("report/view");

        StudentInfo studentInfo = studentService.getDetailedInfo(recordId);
        if (studentInfo == null) {
            model.setStatus(HttpStatus.NOT_FOUND);
            model.setViewName("student/idNotFound");
            return model;
        }

        model.addObject("student", studentInfo);

        TaskInfo taskInfo = taskService.getTaskByStudent(recordId);
        if (taskInfo != null) {
            model.addObject("task", taskInfo);
        }
        return model;
    }


}
