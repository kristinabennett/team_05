package ru.edu.project.backend.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import ru.edu.project.backend.api.students.StudentForm;
import ru.edu.project.backend.api.students.StudentInfo;
import ru.edu.project.backend.api.students.StudentService;
import ru.edu.project.backend.service.StudentServiceLayer;

import java.util.List;

@RestController
@RequestMapping("/student")
public class StudentController implements StudentService {

    /**
     * Делегат для передачи вызова.
     */
    @Autowired
    private StudentServiceLayer delegate;

    /**
     * Добавление/редактирование данных студента.
     *
     * @param studentForm
     * @return StudentInfo
     */
    @Override
    @PostMapping("/editStudent")
    public StudentInfo editStudent(@RequestBody final StudentForm studentForm) {
        return delegate.editStudent(studentForm);
    }

    /**
     * Просмотр информации о студенте.
     *
     * @param recordId
     * @return StudentInfo
     */
    @Override
    @GetMapping("/getDetailedInfo/{recordId}")
    public StudentInfo getDetailedInfo(@PathVariable("recordId") final Long recordId) {
        return delegate.getDetailedInfo(recordId);
    }

    /**
     * Вывод всех студентов.
     *
     * @return List
     */
    @Override
    @GetMapping("/getAllStudents")
    public List<StudentInfo> getAllStudents() {
        return delegate.getAllStudents();
    }

    /**
     * Вывод всех студентов по номеру группы.
     *
     * @param groupId
     * @return List
     */
    @Override
    @GetMapping("/getStudentsByGroup/{groupId}")
    public List<StudentInfo> getStudentsByGroup(@PathVariable("groupId") final Long groupId) {
        return delegate.getStudentsByGroup(groupId);
    }
}
