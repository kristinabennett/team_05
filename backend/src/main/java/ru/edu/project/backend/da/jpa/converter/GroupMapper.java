package ru.edu.project.backend.da.jpa.converter;

import org.mapstruct.Mapper;
import ru.edu.project.backend.api.groups.GroupInfo;
import ru.edu.project.backend.da.jpa.entity.GroupEntity;

import java.util.List;

@Mapper(componentModel = "spring")
public interface GroupMapper {
    /**
     * Маппер GroupInfo -> GroupEntity.
     *
     * @param groupInfo
     * @return entity
     */
    GroupEntity map(GroupInfo groupInfo);

    /**
     * Маппер GroupEntity -> GroupInfo.
     *
     * @param entity
     * @return groupInfo
     */
    GroupInfo map(GroupEntity entity);

    /**
     * Маппер List<GroupEntity> -> List<GroupInfo>.
     *
     * @param listEntity
     * @return list groupInfo
     */
    List<GroupInfo> mapList(Iterable<GroupEntity> listEntity);
}
