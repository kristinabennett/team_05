package ru.edu.project.backend.da.jpa.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import ru.edu.project.backend.da.jpa.entity.SubjectEntity;

import java.util.List;

@Repository
public interface SubjectEntityRepository extends CrudRepository<SubjectEntity, Long> {

    /**
     * запрос всех предметов.
     * @return список
     */
    List<SubjectEntity> findAll();

}
