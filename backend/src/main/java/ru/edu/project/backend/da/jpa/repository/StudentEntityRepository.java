package ru.edu.project.backend.da.jpa.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import ru.edu.project.backend.da.jpa.entity.StudentEntity;

import java.util.List;

@Repository
public interface StudentEntityRepository extends CrudRepository<StudentEntity, Long> {

    /**
     * Поиск записей по полю record_id.
     *
     * @param groupId
     * @return list entity
     */
    List<StudentEntity> findByGroupId(Long groupId);

    /**
     * Поиск записей по полю record_id.
     *
     * @param recordId
     * @return list entity
     */
    StudentEntity findStudentEntityByRecordId(Long recordId);

}
