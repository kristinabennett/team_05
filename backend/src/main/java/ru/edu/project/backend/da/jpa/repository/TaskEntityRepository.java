package ru.edu.project.backend.da.jpa.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import ru.edu.project.backend.da.jpa.entity.TaskEntity;

import java.util.List;

@Repository
public interface TaskEntityRepository extends CrudRepository<TaskEntity, Long> {


    /**
     * поиск по студенту.
     * @param studentId
     * @return список
     */
    List<TaskEntity> findAllByStudentId(long studentId);

    /**
     * запрос всех заданий.
     * @return список
     */
    List<TaskEntity> findAll();

}
