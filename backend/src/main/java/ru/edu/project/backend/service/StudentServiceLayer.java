package ru.edu.project.backend.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;
import ru.edu.project.backend.api.students.StudentForm;
import ru.edu.project.backend.api.students.StudentInfo;
import ru.edu.project.backend.api.students.StudentService;
import ru.edu.project.backend.da.StudentDALayer;

import java.time.LocalDate;
import java.util.List;

@Service
@Profile("!STUB")
@Qualifier("StudentServiceLayer")
public class StudentServiceLayer implements StudentService {

    /**
     * Для выделения года.
     */
    public static final int CENTURY = 100;

    /**
     * Зависимость для слоя доступа к данным групп.
     */
    @Autowired
    private StudentDALayer daLayer;

    /**
     * Добавление/редактирование данных студента.
     *
     * @param studentForm
     * @return StudentInfo
     */
    @Override
    public StudentInfo editStudent(final StudentForm studentForm) {
        StudentInfo draft = StudentInfo.builder()
                .recordId(studentForm.getRecordId())
                .groupId(studentForm.getGroupId())
                .name(studentForm.getName())
                .secondName(studentForm.getSecondName())
                .lastName(studentForm.getLastName())
                .birthdayDate(studentForm.getBirthdayDate())
                .phone(studentForm.getPhone())
                .entryDate(LocalDate.now())
                .build();
        return daLayer.save(draft);
    }

    /**
     * Просмотр информации о студенте.
     *
     * @param recordId
     * @return StudentInfo
     */
    @Override
    public StudentInfo getDetailedInfo(final Long recordId) {
        return daLayer.getById(recordId);
    }

    /**
     * Вывод всех студентов.
     *
     * @return List
     */
    @Override
    public List<StudentInfo> getAllStudents() {
        return daLayer.getAll();
    }

    /**
     * Вывод всех студентов по номеру группы.
     *
     * @param groupId
     * @return List
     */
    @Override
    public List<StudentInfo> getStudentsByGroup(final Long groupId) {
        return daLayer.getStudentsByGroup(groupId);
    }

    /**
     * Генерация номера зачетки студента.
     * @param groupId
     * @param idInGroup
     * @return Long
     */
    public static Long createRecordId(final Long groupId, final Long idInGroup) {
        String recordId = String.valueOf(LocalDate.now().getYear() % CENTURY)
                + String.format("%03d%03d", groupId, idInGroup);
        return Long.parseLong(recordId);
    }
}
