package ru.edu.project.backend.api.students;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.jackson.Jacksonized;

import java.time.LocalDate;
import java.util.HashMap;

@Getter
@Setter
@Builder
@Jacksonized
public class StudentInfo {
    /**
     * id группы студента.
     */
    private Long groupId;

    /**
     * Номер зачетки студента.
     */
    private Long recordId;

    /**
     * Имя студента.
     */
    private String name;

    /**
     * Отчество студента.
     */
    private String secondName;

    /**
     * Фамилия студента.
     */
    private String lastName;

    /**
     * Дата рождения студента.
     */
    private LocalDate birthdayDate;

    /**
     * Контактный номер телефона.
     */
    private String phone;

    /**
     * Дата поступления студента.
     */
    private LocalDate entryDate;

    /**
     * Ведомость успеваемости студента.
     */
    private HashMap<Integer, String> reportCard;

}
