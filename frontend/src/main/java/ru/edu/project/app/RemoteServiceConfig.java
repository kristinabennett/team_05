package ru.edu.project.app;

import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.context.annotation.Scope;
import org.springframework.web.client.RestTemplate;
import ru.edu.project.backend.RestServiceInvocationHandler;
import ru.edu.project.backend.api.groups.GroupService;
import ru.edu.project.backend.api.students.StudentService;
import ru.edu.project.backend.api.tasks.TaskService;
import ru.edu.project.backend.api.user.UserService;

import java.lang.reflect.Proxy;

@Configuration
@Profile("REST")
@SuppressWarnings("unchecked")
public class RemoteServiceConfig {

    /**
     * Создаем rest-прокси для RequestService.
     *
     * @param handler
     * @return rest-proxy
     */
    @Bean
    public StudentService studentServiceRest(final RestServiceInvocationHandler handler) {
        handler.setServiceUrl("/student");
        return getProxy(handler, StudentService.class);
    }

    /**
     * Создаем rest-прокси для GroupService.
     *
     * @param handler
     * @return rest-proxy
     */
    @Bean
    public GroupService groupServiceRest(final RestServiceInvocationHandler handler) {
        handler.setServiceUrl("/group");
        return getProxy(handler, GroupService.class);
    }


    /**
     * Создаем rest-прокси для TaskService.
     *
     * @param handler
     * @return rest-proxy
     */
    @Bean
    public TaskService taskService(final RestServiceInvocationHandler handler) {
        handler.setServiceUrl("/task");
        return getProxy(handler, TaskService.class);
    }

    /**
     * Создаем rest-прокси для RequestService.
     *
     * @param handler
     * @return rest-proxy
     */
    @Bean
    public UserService userService(final RestServiceInvocationHandler handler) {
        handler.setServiceUrl("/user");
        return getProxy(handler, UserService.class);
    }

    /**
     * Bean rest template.
     *
     * @return RestTemplate
     */
    @Bean
    @Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
    public RestTemplate restTemplateBean() {
        return new RestTemplate();
    }


    private <T> T getProxy(final RestServiceInvocationHandler handler, final Class<T>... tClass) {
        return (T) Proxy.newProxyInstance(this.getClass().getClassLoader(), tClass, handler);
    }

}
