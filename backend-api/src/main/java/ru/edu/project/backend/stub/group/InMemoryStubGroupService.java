package ru.edu.project.backend.stub.group;

import ru.edu.project.backend.api.groups.GroupForm;
import ru.edu.project.backend.api.groups.GroupInfo;
import ru.edu.project.backend.api.groups.GroupService;
import ru.edu.project.backend.api.students.StudentService;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicLong;

public class InMemoryStubGroupService implements GroupService {

    /**
     * Ссылка на сервис студентов.
     */
    private StudentService studentService;

    /**
     * Локальное хранилище данных в RAM.
     */
    private Map<Long, GroupInfo> db = new ConcurrentHashMap<>();

    /**
     * Локальный счетчик записей.
     */
    private AtomicLong idCount = new AtomicLong(0);

    /**
     * Конструктор с зависимостью.
     *
     * @param bean
     */
    public InMemoryStubGroupService(final StudentService bean) {
        studentService = bean;
    }

    /**
     * Добавление/редактирование данных группы.
     *
     * @param groupForm
     * @return StudentInfo
     */
    @Override
    public GroupInfo editGroup(final GroupForm groupForm) {
        Long id;
        if (groupForm.getGroupId() == null) {
            id = idCount.addAndGet(1);
        } else {
            id = groupForm.getGroupId();
        }
        GroupInfo info = GroupInfo.builder()
                .groupId(id)
                .name(groupForm.getName())
                .description(groupForm.getDescription())
                .build();

        db.put(id, info);
        return info;
    }

    /**
     * Просмотр информации о студенте.
     *
     * @param groupId
     * @return StudentInfo
     */
    @Override
    public GroupInfo getDetailedInfo(final Long groupId) {
        if (db.containsKey(groupId)) {
            GroupInfo groupInfo = db.get(groupId);
            groupInfo.setStudentList(studentService.getStudentsByGroup(groupId));
            return groupInfo;
        } else {
            return null;
        }
    }

    /**
     * Вывод всех групп.
     *
     * @return List
     */
    @Override
    public List<GroupInfo> getAllGroups() {
        return new ArrayList<GroupInfo>(db.values());
    }
}
