package ru.edu.project.backend.da.jpa.entity;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.sql.Timestamp;

@Entity
@Getter
@Setter
@Table(name = "task")
public class TaskEntity {

    /**
     * id задания.
     */
    @Id
    private Long id;

    /**
     * id студента.
     */
    @Column(name = "student_id")
    private Long studentId;

    /**
     * статус задания.
     */
    @Column(name = "status")
    private String status;

    /**
     * Время создания задания.
     */
    @Column(name = "created_at")
    private Timestamp createdAt;

    /**
     * Дата сдачи контрольных работ.
     */
    @Column(name = "dead_line")
    private Timestamp deadLine;

    /**
     * Комментарий преподавателя.
     */
    @Column(name = "comment")
    private String comment;

    /**
     * parent id.
     *
     * null - это основное задание,
     * not null - это предмет
     */
    @Column(name = "parent_id")
    private Long parentId;

    /**
     * Наименование основного задания либо
     * наименование предмета.
     */
    @Column(name = "task_subtask")
    private String taskSubtask;

    /**
     * Выставленная оценка.
     *
     * null для основного задания,
     * not null для предмета
     */
    @Column(name = "assessment")
    private Long assessment;

}
