package ru.edu.project.backend.da.jpa.entity;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Getter
@Setter
@Table(name = "subject_type")
public class SubjectEntity {

    /**
     * Код предмета.
     */
    @Id
    private Long id;

    /**
     * Название предмета.
     */
    @Column(name = "subject")
    private String title;

    /**
     * Описание предмета.
     */
    @Column(name = "desc")
    private String desc;

    /**
     * Активация предмета.
     */
    @Column(name = "enabled")
    private boolean enabled;

}
